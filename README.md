# Qt Creator bug example

This repository contains code that the clang code model will fail to handle properly when a kit using MSVC 2017 is active.

A kit using MinGW 5.3 works as expected though.

# Clone

To completely clone all dependencies run `git clone` followed by `git submodule update --init --recursive`